document.addEventListener('DOMContentLoaded', function(){ 
    const sections = document.querySelectorAll(".template__section");
    const menu_links = document.querySelectorAll(".navigation__item a");

    const makeActive = (link) => menu_links[link].classList.add("navigation__link--active");
    const removeActive = (link) => menu_links[link].classList.remove("navigation__link--active");
    const removeAllActive = () => [...Array(sections.length).keys()].forEach((link) => removeActive(link));
    
    const sectionMargin = 200;
    let currentActive = 0;
  
    window.addEventListener("scroll", () => {
    const current = sections.length - [...sections].reverse().findIndex((section) => window.scrollY >= section.offsetTop - sectionMargin ) - 1
        if (current !== currentActive) {
            removeAllActive();
            currentActive = current;
            makeActive(current);
        }
    });

    const navigationSwitch = document.querySelector(".navigation__switch--js");
    const navigationList = document.querySelector(".navigation__list");
    let menuOpened = false;

    navigationSwitch.addEventListener("click", (e) => {
        if(menuOpened == false) {
            navigationList.classList.add("navigation__list--visable");
            navigationList.classList.remove("navigation__list--hidden");
            navigationSwitch.innerHTML = "ⅹ";
            menuOpened = true;
        } else {
            navigationList.classList.add("navigation__list--hidden");
            navigationList.classList.remove("navigation__list--visable");
            navigationSwitch.innerHTML = "≡";
            menuOpened = false;
        }
    });

    const navigationLinks = document.querySelectorAll(".navigation__link");

    navigationLinks.forEach(link => {
        link.addEventListener("click", (e) => {
            navigationList.classList.add("navigation__list--hidden");
            navigationList.classList.remove("navigation__list--visable");
            navigationSwitch.innerHTML = "≡";
            menuOpened = false;
        });
    });
    
  }, false);